@echo off
rem 无环境变量的需要设置一下路径
set condaRoot=D:\ProgramData\Anaconda3\
call %condaRoot%\Scripts\activate.bat
call conda activate momo   
set curr_dir=%~dp0
start cmd /k "cd /d %curr_dir%\www\ && python manage.py runserver"
start cmd /k "cd /d %curr_dir%\www\ && python manage.py persistence_hit_log"
start cmd /k "cd /d %curr_dir% && python risk_server.py"